#!/usr/bin/bash

# Clean up to ensure x starts
rm /tmp/.X0-lock &>/dev/null || true

# Run balena base image entrypoint script
/usr/bin/entry.sh echo "Running balena base image entrypoint..."

# Give user permissions to start X
sed -i -e 's/console/anybody/g' /etc/X11/Xwrapper.config
echo "needs_root_rights=yes" >> /etc/X11/Xwrapper.config

# Start x with the dm user
su - dm -c "startx"

while :
do
  echo "Start failed!"
  sleep 30
done
