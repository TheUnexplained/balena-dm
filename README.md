# Balena DM

Balena dual monitor sample codebase.  This codebase is to demonstrate the issue identified here: https://forums.balena.io/t/dual-displays-not-working-using-v2-87-16-rev1-or-newer/350557

## Hardware

1. Raspberry Pi 4
2. Official Raspberry Pi 7 inch Touchscreen
3. HDMI monitor
4. Keyboard
5. Mouse
6. Wires and cables to connect devices to Raspberry Pi

## How to Reproduce Issue

Login: https://dashboard.balena-cloud.com/

1. Create Fleet
2. Navigate to the configuration screen for the fleet
3. Add custom variable:
- BALENA_HOST_CONFIG_max_framebuffers=2
4. Activate variable:
- Activate rainbow splash screen
- Disable rainbow splash screen (useful for debugging as always displays on both screens before boot into Balena)
5. Build and deploy codebase:
- Clone this code base locally
- Deploy to balena with the following CLI commands (&lt;organization&gt; and &lt;fleet&gt; replaced with your values):
- balena login
- balena deploy &lt;organization&gt;/&lt;fleet&gt;
6. Add device:
- Device type: Raspberry Pi 4 (using 64bit OS)
- Version (select version you want to test): v2.85.2+rev3
7. Items of interest during boot up:
- Rainbow splash screen displays on both monitors regardless of version
- Version v2.87.16+rev1 and newer only displays the Balena logo on one screen
- Version v2.85.2+rev3 and older displays the Balena logo on both screens
8. Once booted into x:
- Right click and open terminal then execute the following commands to view the detected displays and supported resolutions:
- tvservice -l
- xrandr
9. When using v2.87.16+rev1 and newer the dual monitors do not work, but dual monitors work with v2.85.2+rev3 and older.

## Alternative to Reproduce Issue Without Code Deploy

You do not have to clone this codebase and deploy it to reproduce the issue.  I noticed that the rainbow splash screen (when activated and disabled) always displays on both screens (when the max framebuffers are set to 2) then if you have the issue with the version, the Balena logo will only display on one monitor.  When the Balena logo displays on both monitors you do not have the issue.

## Outcome

Please update the issue (link above) if you find a way to enable dual monitors with v2.87.16+rev1 or newer as I would like to use to the recommended version when adding a device.  Many thanks in advance!
